// :62090

const Koa = require('koa')
const bodyParser = require('koa-bodyparser')
const Telegraf = require('telegraf')
const Markup = require('telegraf/markup')

const store = require('json-store')(process.env.DB || __dirname + '/store.json')

const app = new Koa

app.use(bodyParser())
app.use(async ctx => {
  ctx.body = ctx.request.body
})


const SECRET = process.env.SECRET

const bot = new Telegraf(process.env.TOKEN)

bot.context.getUser = async (message) =>
  new Promise((resolve, reject) =>
    resolve(store.get(message.from.username))
  )


bot.use(async ({message, state, getUser }, next) => {
  state.user = await getUser(message).catch(e => e)
  return next()
})

bot.command('auth', ({ replyWithMarkdown, reply, message, chat}) => {
  const pwd = (message.text.split(/\s+/)[1] || "").trim()
  if(pwd === SECRET) {
    store.set(message.from.username, {
      authorized: true,
      chat,
    })
    replyWithMarkdown('Welcome!')
    replyWithMarkdown('Next: /yt `YOUTRACK_USERNAME`')
  } else {
    reply('💩💩💩')
  }
})

bot.command('yt', async ({ replyWithMarkdown, state, message }) => {
  const { user } = state
  const ytid = (message.text.split(/\s+/)[1] || "").trim()
  if (!ytid) {
    return replyWithMarkdown('wrong yt')
  }
  store.set(message.from.username, { ...user, ytid })
  replyWithMarkdown(`done ${ytid}`)
})


bot.use(async ({ replyWithMarkdown, message, state, getUser}, next) => {
  const {user} = state
  if(!user) {
    return replyWithMarkdown('/auth `PASSWORD`')
  }
  if(!user.ytid) {
    return replyWithMarkdown('/yt `YOUTRACK username`')
  }
  return next()
})


bot.command('test', async ({ replyWithMarkdown, state, message }) => {
  const { user } = state
})

bot.command('ping', async ({ reply, state, message }) => {
  reply('pong')
})



app.listen(process.env.PORT || 3000)
bot.startPolling()
// bot.telegram.setWebhook(process.env.URL)